import xlrd
from bin import Bill
#import Bill

class Parser():
    def __init__(self, path):

        self.sales = open("satislar.csv","w", encoding="windows-1254")
        self.returns = open("iadeler.csv", "w", encoding="windows-1254")

        self.write_csv_header(self.sales)
        self.write_csv_header(self.returns)
        
        self.file = xlrd.open_workbook(path)
        self.worksheet = self.file.sheet_by_index(0)

    def parse(self):
        import datetime
        
        number_of_rows =  self.worksheet.nrows
        current_row = 1

        while current_row < number_of_rows:
            quantity = self.worksheet.cell(current_row, 46).value

            if quantity == 0:
                current_row += 1
                continue
            
            date = self.worksheet.cell(current_row, 1).value
            date = datetime.datetime(*xlrd.xldate_as_tuple(date, self.file.datemode))
            bill = Bill.Bill(date,
                             self.worksheet.cell(current_row, 7).value,
                             self.worksheet.cell(current_row, 14).value,
                             self.worksheet.cell(current_row, 12).value,
                             self.worksheet.cell(current_row, 70).value,
                             self.worksheet.cell(current_row, 71).value,
                             self.worksheet.cell(current_row, 73).value)
            bill.set_type(quantity)

            yield bill

            current_row += 1

    def write_csv_header(self, file):
        file.write("Tarih;FaturaNo;Açıklama;Matrah;%8 Kdv Tutarı;GenelToplam")


    def get_bill_dicts(self):
        sales  = {}
        returns = {}

        t_dict = None
        for bill in self.parse():
            
            if bill.type == 1: t_dict = sales

            elif bill.type == -1: t_dict = returns
            
            else:
                print("Unknown Type", bill._id)
                continue

            
            if bill._id in t_dict:
                t_dict[bill._id] += bill
            else:
                t_dict[bill._id] = bill

        return sales, returns

    def compress_bills(self, bills, compress_factor):
        new_bills = []
        old_bill = Bill.Bill("0", 0, "-", "-", 0, 0, 0)
        tmp_bill = None
        
        for bill in bills:
            if old_bill.date == bill.date:
                tmp_bill.merge(bill)
            else:
                new_bills.append(tmp_bill)
                tmp_bill =  bill
                
            old_bill = bill

        del new_bills[0]    
        return new_bills

    def calculate_babs(self, bills):
        customers = {}
        
        for bill in bills:
            if not bill.customer_id in customers:
                customers[bill.customer_id] = bill.base_amount
            else:
                customers[bill.customer_id] += bill.base_amount

        babs = []
        for customer in customers:
            if customers[customer] >= 5000:
                babs.append((customer, customers[customer]))

        return babs
    
    def write_to_csv_file(self, bills):
        for bill in bills:
            if bill.type == 1:
                self.sales.write(bill.serialize())
            elif bill.type == -1:
                self.returns.write(bill.serialize())


if __name__ == "__main__":
    import time

    
    p = Parser("dd.xlsx")
    
    a, b = p.get_bill_dicts()
    
    print(len(a), len(b))

    start = time.time()
    c1 = p.compress_bills(a.values(),10)
    print("Sıkıştırıldı")
    c2 = p.compress_bills(b.values(),12)
    print("Sıkıştırıldı")
    

    
    p.write_to_csv_file(c1)
    p.write_to_csv_file(c2)
    stop = time.time()

    print(p.calculate_babs(c1))
    print(p.calculate_babs(c2))

    
    p.sales.close()
    p.returns.close()

    print(stop-start)
