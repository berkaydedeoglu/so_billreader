class Bill:
    def __init__(self, date, _id, comment, customer_id, base_amount, tax_amount, total_amount):
        self.date = date
        self._id = str(int(_id))
        self.comment = comment
        self.customer_id = customer_id
        self.base_amount = base_amount
        self.tax_amount = tax_amount
        self.total_amount = total_amount
        self.type = 0

    def serialize(self):
        date = self.date.strftime("%d.%m.%Y")
        base = self._tr_float(self.base_amount)
        tax = self._tr_float(self.tax_amount)
        total = self._tr_float(self.total_amount)
        serialized_text = "\n{0};{1};{2};{3};{4};{5}".format(date, self._id,
                                                                 self.comment,
                                                                 base, tax,
                                                                 total)

        return serialized_text


    def _tr_float(self, number):
        text = str(number).replace(".", ",")

        # text = text.replace(",","*")
        # text = text.replace(".", ",")
        # text = text.replace("*", ".")
        
        return text

    def set_type(self, type):
        self.type = type
        if type == -1:
            self.base_amount *= -1
            self.tax_amount *= -1
            self.total_amount *= -1

    def merge(self, other_bill):
        # vex = self.id.find("-")
        # if not vex == -1:

        s = "-"
        self._id = s.join((self._id.split('-')[0], other_bill._id))

        self.comment = "Aynı Gün Faturaları Birleştirildi"
        self.base_amount += other_bill.base_amount
        self.tax_amount += other_bill.tax_amount
        self.total_amount += other_bill.total_amount

        return self

    def __iadd__(self, other_bill):
        if not (self._id == other_bill._id and self.type == other_bill.type):
            raise ValueError("Bill ID or bill type could not be matched.")

        #self.comment = "birlestirildi"
        self.base_amount += other_bill.base_amount
        self.tax_amount += other_bill.tax_amount
        self.total_amount += other_bill.total_amount

        return self
        

if __name__ ==  "__main__":
    import datetime
    b = Bill(datetime.datetime.now(), 12345, "Berkay Dedeoğlu", 123456, 12.45, 42.43, 54.53)
    a = Bill(datetime.datetime.now(), 12345, "Berkay Dedeoğlu", 123456, 12.45, 42.43, 54.53)
    # a.set_type(-1)
    b += a
    print(b.base_amount)
    print(a.base_amount)
