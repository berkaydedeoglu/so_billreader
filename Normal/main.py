from bin import Parser, UI


def operation_starter(window):
    import time

    window.compress_flag.set(1)
    
    window.start_button["state"] = "disabled"
        
    send_message(window, "Dosya Ayrıştırılıyor...")
    parser = Parser.Parser(window.get_file_path())
    send_message(window, "Dosya Başarıyla Ayrıştırıldı!")

    time.sleep(0.5)
    
    send_message(window, "Faturalar düzenleniyor...")
    sales, returns, posets = parser.get_bill_dicts()
    time.sleep(3)
    send_message(window, "Faturalar düzenlendi!")
    
    send_message(window, "Poşetler düzenleniyor...")
    time.sleep(2)
    send_message(window, str(len(posets)) + " adet poset bulundu")
    time.sleep(1) 
    
    send_message(window, "Aynı numaralı faturalar birleştiriliyor...")
    time.sleep(2)
    send_message(window, "Aynı numaralı faturalar birleştirildi!")

    
    send_message(window, "satislar.csv dosyası oluşturuluyor...")
    parser.write_to_csv_file(sales.values())
    time.sleep(1)
    send_message(window, "satislar.csv dosyası oluşturuldu!")

    send_message(window, "iadeler.csv dosyası oluşturuluyor...")
    parser.write_to_csv_file(returns.values())
    time.sleep(1)
    send_message(window, "iadeler.csv dosyası oluşturuldu!")
    
    send_message(window, "poset.csv dosyası oluşturuluyor...")
    parser.write_to_csv_file(posets.values())
    time.sleep(1)
    send_message(window, "poset.csv dosyası oluşturuldu!")

    if window.babs_flag.get() == 1:
        send_message(window, "Satışlar için BS kontrol ediliyor..")
        time.sleep(0.7)
        s_babs = parser.calculate_babs(sales.values())
        if len(s_babs) == 0:
            send_message(window, "---BS YOK---")
        else:
            send_message(window, "--------BS--------")
            for i in s_babs:
                send_message(window, i[0] + "nolu müşteri:" + str(i[1]) + "TL")

        send_message(window, "BS kontrolü yapıldı!")

        send_message(window, "İadeler için BA kontrol ediliyor...")

        i_babs = parser.calculate_babs(returns.values())
        if len(i_babs) == 0:
            send_message(window, "---BA YOK---")
        else:
            send_message(window, "--------BA--------")
            for i in i_babs:
                send_message(window, i[0] + "nolu müşteri:" + str(i[1]) + "TL")

        send_message(window, "BA kontrolü yapıldı!")

    time.sleep(1)
    send_message(window, "----------------------------")
    send_message(window, "----------------------------")
    send_message(window, "İşlemler tamamlandı. MUTLAKA KONTROL EDİN")
    send_message(window, "MUTLAKA KONTROL EDİN")
    
    window.start_button["text"] = "---LÜTFEN KONTROL EDİN---"
    window.start_button["bg"] = "#ff0066"
    window.start_button["fg"] = "#ffffff"
    

def send_message(window, message):
    window.insert_message(message+"\n")

def main():
    window = UI.UserInterface(operation_starter)
    window.mainloop()

main()
