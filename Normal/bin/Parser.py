import xlrd
from bin import Bill

class Parser():
    def __init__(self, path):

        self.sales = open("satislar.csv","w", encoding="windows-1254")
        self.returns = open("iadeler.csv", "w", encoding="windows-1254")
        self.posets = open("poset.csv", 'w', encoding="windows-1254")

        self.write_csv_header(self.sales, '8')
        self.write_csv_header(self.returns, '8')
        self.write_csv_header(self.posets, '18')
        
        self.file = xlrd.open_workbook(path)
        self.worksheet = self.file.sheet_by_index(0)

    def parse(self):
        import datetime
        
        number_of_rows =  self.worksheet.nrows
        current_row = 1

        while current_row < number_of_rows:
            quantity = self.worksheet.cell(current_row, 46).value
            is_poset = False

            if quantity == 0:
                current_row += 1
                continue
                
            
            date = self.worksheet.cell(current_row, 1).value
            date = datetime.datetime(*xlrd.xldate_as_tuple(date, self.file.datemode))

            bill = Bill.Bill(date,
                             self.worksheet.cell(current_row, 7).value,
                             self.worksheet.cell(current_row, 14).value,
                             self.worksheet.cell(current_row, 12).value,
                             self.worksheet.cell(current_row, 70).value,
                             self.worksheet.cell(current_row, 71).value,
                             self.worksheet.cell(current_row, 73).value)
            bill.set_type(quantity)
            
            # comment = self.worksheet.cell(current_row, 36).value

            # Eğer ürün %18'lik ise poşettir. Bu satır 27.04.2020 tarihinda Aslı Subaşı
            # isteği ile değiştirilmiştir.
            tax_type = self.worksheet.cell(current_row, 53).value
            if tax_type == 18:
                bill.set_type(-65) # Bu bir posettir
            
            yield bill
            current_row += 1

    def write_csv_header(self, file, vat):
        file.write("Tarih;FaturaNo;Açıklama;Matrah;%"+ vat + " Kdv Tutarı;GenelToplam")


    def get_bill_dicts(self):
        sales  = {}
        returns = {}
        posets = {}

        t_dict = None
        for bill in self.parse():
            
            if bill.type == 1: t_dict = sales

            elif bill.type == -1: t_dict = returns
            
            elif bill.type == -65: t_dict = posets
            
            else:
                print("Unknown Type", bill.id)
                continue

            
            if bill.id in t_dict:
                t_dict[bill.id] += bill
            else:
                t_dict[bill.id] = bill

        return sales, returns, posets

    def compress_bills(self, bills, compress_factor):
        pass

    def calculate_babs(self, bills):
        customers = {}
        
        for bill in bills:
            if not bill.customer_id in customers:
                customers[bill.customer_id] = bill.base_amount
            else:
                customers[bill.customer_id] += bill.base_amount

        babs = []
        for customer in customers:
            if customers[customer] >= 5000:
                babs.append((customer, customers[customer]))

        return babs
    
    def write_to_csv_file(self, bills):
        for bill in bills:
            if bill.type == 1:
                self.sales.write(bill.serialize())
            elif bill.type == -1:
                self.returns.write(bill.serialize())
            elif bill.type == -65:
                self.posets.write(bill.serialize())

        


if __name__ == "__main__":
    import time

    
    p = Parser("dd.xlsx")
    
    a, b = p.get_bill_dicts()
    
    print(len(a), len(b))

    start = time.time()
    p.write_to_csv_file(a.values())
    p.write_to_csv_file(b.values())
    stop = time.time()

    print(p.calculate_babs(a.values()))
    print(p.calculate_babs(b.values()))

    
    p.sales.close()
    p.returns.close()

    print(stop-start)
