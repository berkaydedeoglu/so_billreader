from bin import Parser, UI


def operation_starter(window):
    import time

    window.compress_flag.set(1)
    
    window.start_button["state"] = "disabled"
        
    send_message(window, "Dosya Ayrıştırılıyor...")
    parser = Parser.Parser(window.get_file_path())
    send_message(window, "Dosya Başarıyla Ayrıştırıldı!")

    time.sleep(0.5)
    
    send_message(window, "Faturalar düzenleniyor...")
    sales, returns = parser.get_bill_dicts()
    time.sleep(0.5)
    send_message(window, "Faturalar düzenlendi!")
    
    send_message(window, "Aynı numaralı faturalar birleştiriliyor...")
    time.sleep(0.5)
    send_message(window, "Aynı numaralı faturalar birleştirildi!")
    
    if window.babs_flag.get() == 1:
        send_message(window, "Satışlar için BS kontrol ediliyor..")
        time.sleep(0.7)
        s_babs = parser.calculate_babs(sales.values())
        if len(s_babs) == 0:
            send_message(window, "---BS YOK---")
        else:
            send_message(window, "--------BS--------")
            for i in s_babs:
                send_message(window, i[0] + "nolu müşteri:" + str(i[1]) + "TL")

        send_message(window, "BS kontrolü yapıldı!")

        send_message(window, "İadeler için BA kontrol ediliyor...")

        i_babs = parser.calculate_babs(returns.values())
        if len(i_babs) == 0:
            send_message(window, "---BA YOK---")
        else:
            send_message(window, "--------BA--------")
            for i in i_babs:
                send_message(window, i[0] + "nolu müşteri:" + str(i[1]) + "TL")

        send_message(window, "BA kontrolü yapıldı!")
    
    s_li = sales.values()
    r_li = returns.values()
    if window.ultra_compress_flag.get() == 1:
        send_message(window, "Aynı gün faturaları birleştiriliyor...")
        s_li = parser.compress_bills(sales.values(), 20)
        r_li = parser.compress_bills(returns.values(), 20)
        send_message(window, "Aynı gün faturaları birleştirildi.")
		

    
    send_message(window, "satislar.csv dosyası oluşturuluyor...")
    parser.write_to_csv_file(s_li)
    time.sleep(1)
    send_message(window, "satislar.csv dosyası oluşturuldu!")

    send_message(window, "iadeler.csv dosyası oluşturuluyor...")
    parser.write_to_csv_file(r_li)
    time.sleep(0.2)
    send_message(window, "iadeler.csv dosyası oluşturuldu!")

    time.sleep(1)
    send_message(window, "İşlemler tamamlandı. MUTLAKA KONTROL EDİN")
    send_message(window, "MUTLAKA KONTROL EDİN")
    
    window.start_button["text"] = "---LÜTFEN KONTROL EDİN---"
    window.start_button["bg"] = "#ff0066"
    window.start_button["fg"] = "#ffffff"

    

def send_message(window, message):
    window.insert_message(message+"\n")

def main():
    window = UI.UserInterface(operation_starter)
    window.mainloop()

if __name__ == "__main__":
    main()
