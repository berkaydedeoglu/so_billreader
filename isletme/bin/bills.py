from bin import Bill

class BillSale18(Bill.Bill):
    def __init__(self, date, id, comment, customer_id, base_amount,
                 tax_amount, total_amount, serial):
        super().__init__(date, id, comment, customer_id, base_amount,
                 tax_amount, total_amount, serial)
                 
    def serialize(self):
        date = self.date.strftime("%d/%m/%Y")
        base = self._tr_float(self.base_amount)
        tax = self._tr_float(self.tax_amount)
        total = self._tr_float(self.total_amount)
        
        serialized = f"\nGelir;;Satış;{date};{date};{self.serial};{self.id};"
        serialized += f";;{self.comment};{self.comment};;;;;Fatura;"
        serialized += f"Normal Satışlar;Mal Satışı;600.18;{self.comment};"
        serialized += f";;{base};;18;{tax};{total};;;;"
        
        return serialized
        
        
        
class BillSale8(Bill.Bill):
    def __init__(self, date, id, comment, customer_id, base_amount,
                 tax_amount, total_amount, serial):
        super().__init__(date, id, comment, customer_id, base_amount,
                 tax_amount, total_amount, serial)
                 
    def serialize(self):
        date = self.date.strftime("%d.%m.%Y")
        base = self._tr_float(self.base_amount)
        tax = self._tr_float(self.tax_amount)
        total = self._tr_float(self.total_amount)
        
        serialized = f"\nGelir;;Satış;{date};{date};{self.serial};{self.id};"
        serialized += f";;{self.comment};{self.comment};;;;;Fatura;"
        serialized += f"Normal Satışlar;Mal Satışı;600.08;{self.comment};"
        serialized += f";;{base};;8;{tax};{total};;;;"
        
        return serialized




class BillReturn18(Bill.Bill):
    def __init__(self, date, id, comment, customer_id, base_amount,
                 tax_amount, total_amount, serial):
        super().__init__(date, id, comment, customer_id, base_amount,
                 tax_amount, total_amount, serial)
                 
    def serialize(self):
        date = self.date.strftime("%d.%m.%Y")
        base = self._tr_float(self.base_amount)
        tax = self._tr_float(self.tax_amount)
        total = self._tr_float(self.total_amount)
        
        serialized = f"\nGider;;Satıştan İade;{date};{date};{self.serial};{self.id};"
        serialized += f";;{self.comment};{self.comment};;;;;Gider Pusulası;"
        serialized += f"Satıştan İade;Mal Alışı;610.18;{self.comment};"
        serialized += f";;{base};;18;{tax};{total};;;;"
        
        return serialized



class BillReturn8(Bill.Bill):
    def __init__(self, date, id, comment, customer_id, base_amount,
                 tax_amount, total_amount, serial):
        super().__init__(date, id, comment, customer_id, base_amount,
                 tax_amount, total_amount, serial)
                 
    def serialize(self):
        date = self.date.strftime("%d.%m.%Y")
        base = self._tr_float(self.base_amount)
        tax = self._tr_float(self.tax_amount)
        total = self._tr_float(self.total_amount)
        
        serialized = f"\nGider;;Satıştan İade;{date};{date};{self.serial};{self.id};"
        serialized += f";;{self.comment};{self.comment};;;;;Gider Pusulası;"
        serialized += f"Satıştan İade;Mal Alışı;610.08;{self.comment};"
        serialized += f";;{base};;8;{tax};{total};;;;"
        
        return serialized
