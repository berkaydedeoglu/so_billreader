import xlrd
from bin import Bill
from bin import bills

class Parser():
    def __init__(self, path):

        self.sales = open("satislar.csv","w", encoding="windows-1254")
        self.returns = open("iadeler.csv","w", encoding="windows-1254")
        self.posets = open("poset.csv", 'w', encoding="windows-1254")

        self.write_csv_header(self.sales)
        self.write_csv_header(self.posets)
        self.write_csv_header(self.returns)
        
        self.file = xlrd.open_workbook(path)
        self.worksheet = self.file.sheet_by_index(0)

    def parse(self):
        import datetime
        
        number_of_rows =  self.worksheet.nrows
        current_row = 1

        while current_row < number_of_rows:
            quantity = self.worksheet.cell(current_row, 46).value
            percent = self.worksheet.cell(current_row, 53).value
            is_poset = False

            if quantity == 0:
                current_row += 1
                continue
                
            date = self.worksheet.cell(current_row, 1).value
            date = datetime.datetime(*xlrd.xldate_as_tuple(date, self.file.datemode))
            li = [date,
                 self.worksheet.cell(current_row, 7).value,
                 self.worksheet.cell(current_row, 14).value,
                 self.worksheet.cell(current_row, 12).value,
                 self.worksheet.cell(current_row, 70).value,
                 self.worksheet.cell(current_row, 71).value,
                 self.worksheet.cell(current_row, 73).value,
                 self.worksheet.cell(current_row, 6).value]
            
            bill = None
            if quantity == 1:
                if percent == 8:
                    bill = bills.BillSale8(li[0], li[1], li[2], li[3], li[4], li[5],li[6], li[7])
                elif percent == 18:
                    bill = bills.BillSale18(li[0], li[1], li[2], li[3], li[4], li[5],li[6], li[7])
            elif quantity == -1:
                if percent == 8:
                    bill = bills.BillReturn8(li[0], li[1], li[2], li[3], li[4], li[5],li[6], li[7])
                elif percent == 18:
                    bill = bills.BillReturn18(li[0], li[1], li[2], li[3], li[4], li[5],li[6], li[7])

            bill.set_type(quantity)
            
            # comment = self.worksheet.cell(current_row, 36).value

            # Eğer ürün %18'lik ise poşettir. Bu satır 27.04.2020 tarihinda Aslı Subaşı
            # isteği ile değiştirilmiştir.
            tax_type = self.worksheet.cell(current_row, 53).value
            if tax_type == 18:
                bill.set_type(-65) # Bu bir posettir

            yield bill

            current_row += 1

    def write_csv_header(self, file):
        file.write("İŞLEM;KATEGORİ;BELGE TURU;EVRAK TARİHİ;" + 
                   "KAYIT TARİHİ;SERİ NO;EVRAK NO;TCKN/VKN;" +
                   "VERGİ DAİRESİ;SOYADI ÜNVAN;ADI DEVAMI;" +
                   "ADRES;CARİ HESAP;KDV İSTİSNASI;KOD;" + 
                   "BELGE TÜRÜ(DB);ALIŞ/SATIŞ TÜRÜ;KAYIT ALT TÜRÜ;" + 
                   "MAL VE HİZMET KODU;AÇIKLAMA;MİKTAR;B.FİYAT;" +
                   "TUTAR;TEVKİFAT;KDV ORANI;KDV TUTARI;TOPLAM TUTAR;"+
                   "KREDİLİ TUTAR;STOPAJ KODU;STOPAJ TUTARI;" +
                   "DÖNEMSELLİK İLKESİ")


    def get_bill_dicts(self):
        sales = {}
        returns = {}
        self.poset_dict = {}

        t_dict = None
        for bill in self.parse():
            
            if bill.type == 1: t_dict = sales
            
            elif bill.type == -1: t_dict = returns
            
            elif bill.type == -65: t_dict = self.poset_dict
            
            else:
                print("Unknown Type", bill.id)
                continue

            
            if bill.id in t_dict:
                t_dict[bill.id] += bill
            else:
                t_dict[bill.id] = bill

        print(returns)
        print(sales)

        return sales, returns, self.poset_dict

    def compress_bills(self, bills, compress_factor):
        pass

    def calculate_babs(self, bills):
        customers = {}
        
        for bill in bills:
            if not bill.customer_id in customers:
                customers[bill.customer_id] = bill.base_amount
            else:
                customers[bill.customer_id] += bill.base_amount

        babs = []
        for customer in customers:
            if customers[customer] >= 5000:
                babs.append((customer, customers[customer]))

        return babs
    
    def write_to_csv_file(self, bills):
        for bill in bills:
            if bill.type == 1:
                self.sales.write(bill.serialize())
                bill_id = bill.id
                
                try:
                    poset = self.poset_dict.pop(bill_id)
                except KeyError:
                    poset = None
                
                if not poset == None:
                    self.sales.write(poset.serialize())
                
            elif bill.type == -1:
                self.returns.write(bill.serialize())
                bill_id = bill.id
                
                try:
                    poset = self.poset_dict.pop(bill_id)
                except KeyError:
                    poset = None
                
                if not poset == None:
                    self.returns.write(poset.serialize())
                

            elif bill.type == -65:
                self.sales.write(bill.serialize())

        


if __name__ == "__main__":
    import time

    
    p = Parser("dd.xlsx")
    
    a, b = p.get_bill_dicts()
    
    print(len(a), len(b))

    start = time.time()
    p.write_to_csv_file(a.values())
    p.write_to_csv_file(b.values())
    stop = time.time()

    print(p.calculate_babs(a.values()))
    print(p.calculate_babs(b.values()))

    
    p.sales.close()
    p.returns.close()

    print(stop-start)
