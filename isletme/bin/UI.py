from tkinter import *

class UserInterface(Tk):

    def __init__(self,  operation_starter):
        super().__init__()
        
        self.operation_starter = operation_starter

        self.title("Fatura Düzenleme")
        self.geometry("600x320")

        self.compress_flag = IntVar()
        self.babs_flag = IntVar()
        self.ultra_compress_flag = IntVar()

        self._add_widgets()

    def _add_widgets(self):
        path_frame = Frame()
        path_frame.pack(fill="x", padx=15)
        
        self.path_entry = Entry(path_frame)
        self.path_entry.pack(side="left", expand=True, fill="x")

        path_button = Button(path_frame, text="Dosya Aç", command=self._open_file).pack(side="left", fill="x")

        checks_frame = Frame(self)
        checks_frame.pack(fill="x")

        Checkbutton(checks_frame, text="Ba-Bs", variable=self.babs_flag).pack(expand=1, side="left", fill="x")
        Checkbutton(checks_frame, text="Aynı Numaraları Eşle", variable=self.compress_flag).pack(expand=1, fill="x", side="left")
        Checkbutton(checks_frame, text="Tek Kalem", variable=self.ultra_compress_flag).pack(side="left",expand=1,  fill="x")

        textbox_frame = Frame()
        textbox_frame.pack(fill="x", pady= 20)

        self.state_text = Text(textbox_frame, height=10, bg="#666699", fg="#ffffff")
        self.state_text.pack(fill="x")

        button_frame = Frame()
        button_frame.pack(fill="x")
        self.start_button = Button(button_frame, text="Başla", bg="#F0FFF0", command=self._start_operation)
        self.start_button.pack(fill="x") 

        

    def _open_file(self):
        from tkinter import filedialog

        
        
        file_dialog = filedialog.askopenfilename(initialdir = "./",
                               title = "Dosya Seçin",
                               filetypes = (("Excel Dosyası","*.xlsx"),("Tüm Dosyalar","*.*")))

        self.path_entry.delete(0, END)
        self.path_entry.insert(0, file_dialog)


    def insert_message(self, message):
        self.state_text.insert(END, message)
        self.state_text.see(END)
        self.update()


    def get_file_path(self):
        return self.path_entry.get()

    def _start_operation(self):
        self.operation_starter(self)
        
if __name__ == "__main__":
    ui = UserInterface()
    ui.mainloop()
